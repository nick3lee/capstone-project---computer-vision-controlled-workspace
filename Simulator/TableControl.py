import time
import serial

class TableControl():

    def __init__(self, serial_port='COM16'):
        # Nick : '/dev/cu.wchusbserialfd120'
        # Chris : 'COM16'
        self.arduino_data = serial.Serial(serial_port,9600)
        self.MagDict = {True:str.encode(str(-5),'utf-8'), False: str.encode(str(-6),'utf-8')}

        time.sleep(1)   #  Await connection

        #Wait for calibration routine to finish
        while(self.arduino_data.inWaiting() < 1):
            time.sleep(.1)

        if(self.arduino_data.read(1) == 1):
            print('failed to calibrate')

        else:
            print('calibrated')



    def travel(self, x_coord, y_coord, magState):

        try:
            #Magnet position
            self.arduino_data.write(self.MagDict[magState])
            while self.arduino_data.inWaiting() < 1:
                time.sleep(.05)

            self.arduino_data.read(size=self.arduino_data.inWaiting())
            #---------------------------------------

            #coordinates
            x_pos = str.encode(str(x_coord),'utf-8')
            self.arduino_data.write(x_pos)
            print("sent x")

            while self.arduino_data.inWaiting() < len(x_pos):
                time.sleep(.1)
            response = self.arduino_data.read(size=self.arduino_data.inWaiting())
            #response = self.arduino_data.readline()
            print("response xcoordr:", int(response))


            y_pos = str.encode(str(y_coord), 'utf-8')
            self.arduino_data.write(y_pos)

            print("sent y")

            while self.arduino_data.inWaiting() < len(y_pos):
                time.sleep(.05)

            #response = self.arduino_data.read(size=len(y_pos))
            response = self.arduino_data.read(size=self.arduino_data.inWaiting())
            print("response ycoordr:", int(response))

            while self.arduino_data.inWaiting() < 1:
                time.sleep(.05)

            Success = self.arduino_data.read(size=self.arduino_data.inWaiting())
            print("Successfully added to Queue: ", int(Success))

            print("Done----------------")
        except Exception as e:
            print(e)

    def execute(self):
        execute_code = str.encode(str(-4),'utf-8')
        self.arduino_data.write(execute_code)
        while self.arduino_data.inWaiting() < len(str.encode(str(1),'utf-8')):
            time.sleep(.05)
        response = self.arduino_data.read(size=self.arduino_data.inWaiting())
        print("Was movement successful: ", int(response))


def Convert_to_Coordinate(Table_dims,apos, bpos, bucket ):

    if bpos <= 35 :
        bpos = 3
    if bpos >= Table_dims[1]-35:
        bpos = Table_dims[1]-3

    acoord = int(((apos-Table_dims[2])/Table_dims[0])*1000)
    bcoord = int(((bpos)/Table_dims[1])*1000)

    return acoord, bcoord

def determine_temp_coord(pdir, apos, bpos, vel):
    if pdir == "L" or pdir == "R":
        if pdir == "L":
            apos = apos - vel
        else:
            apos = apos + vel
    else:
        if pdir == "D":
            bpos = bpos + vel
        else:
            bpos = bpos - vel

    return(apos,bpos)


def sort_on_board(table_pos, Buckets, Magpos, vel, Pathfound, table_controller):
    Table_dims = [table_pos[2] - table_pos[0], table_pos[3]-table_pos[1] + Buckets[0].height*2, table_pos[0],table_pos[1]] #table dims
    apos = Magpos[0]
    bpos = Magpos[1] #+ Buckets[0].height

    print("Starting Mag position " + str(apos) + " " + str(bpos))


    coord_list = [Convert_to_Coordinate(Table_dims, apos, bpos, Buckets[0])] #init coord for object
    temp_coord = (apos,bpos)
    lastprt = 0
    currptr = 1
    nptr = 2

    while nptr <= len(Pathfound):
        while nptr != len(Pathfound) and Pathfound[lastprt] == Pathfound[currptr]: #Straight Line
            temp_coord = determine_temp_coord(Pathfound[currptr],temp_coord[0],temp_coord[1],vel)
            lastprt +=1
            currptr += 1
            nptr += 1
            if nptr == len(Pathfound) or Pathfound[lastprt] != Pathfound[currptr]:
                coord_list.append(Convert_to_Coordinate(Table_dims, temp_coord[0], temp_coord[1], Buckets[0]))
        while nptr !=len(Pathfound) and Pathfound[lastprt] == Pathfound[nptr]:  # diagonal
            temp_coord = determine_temp_coord(Pathfound[currptr], temp_coord[0], temp_coord[1], vel)
            lastprt += 1
            currptr += 1
            nptr += 1
            if nptr == len(Pathfound) or Pathfound[lastprt] != Pathfound[nptr]:
                coord_list.append(Convert_to_Coordinate(Table_dims, temp_coord[0], temp_coord[1], Buckets[0]))
        if nptr !=len(Pathfound) and Pathfound[lastprt] !=Pathfound[currptr] and Pathfound[lastprt] != Pathfound[nptr] and Pathfound[currptr] != Pathfound[nptr]:
            temp_coord = determine_temp_coord(Pathfound[currptr], temp_coord[0], temp_coord[1], vel)
            coord_list.append(Convert_to_Coordinate(Table_dims, temp_coord[0], temp_coord[1], Buckets[0]))
            lastprt += 1
            currptr += 1
            nptr += 1
            continue
        lastprt += 1
        currptr += 1
        nptr += 1
        if nptr >= len(Pathfound):
            #print("in break")
            break
    print("NICK UNCOMMENT THIS   TABLE CONTROL.py")
    print(coord_list)
    # now we have to set the magnet
    travel_list =[]
    travel_list.append((coord_list[0][0],coord_list[0][1],False))
    for c in range(1,len(coord_list)):
        travel_list.append((coord_list[c][0], coord_list[c][1], True))

    #ToDo YOU HAVE TO UNCOMMENT THIS
    for trav in travel_list:
        table_controller.travel(int(trav[1]), int(trav[0]), trav[2])
        time.sleep(.1)
    table_controller.execute()
    #table_controller.travel.(0,0,False)
