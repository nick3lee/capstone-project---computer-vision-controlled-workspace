import pygame
import time
import TableControl
import Algorithms


#classes and help functions


#------------Classes -----------------
class Magnet:
    def __init__(self, a, b, width, height, vel):
        self.a = a
        self.b = b
        self.pos = (self.a, self.b)
        self.width = width
        self.height = height
        self.vel = vel
        self.hit = (self.a + self.width/2 , self.b + self.height/2,0,0)

    def draw(self, win):
        self.pos = (self.a, self.b)
        self.hit = (self.a + self.width / 2, self.b + self.height / 2, 0, 0)
        pygame.draw.rect(win, (100, 100, 100), (self.a, self.b, self.width, self.height))  # Magnet change to win.blit(img,position) if wanting to put mag pic in
        pygame.draw.rect(win, (255, 255, 255),self.hit, 1)  # Mag hitbo


class Object(object):
    def __init__(self, a, b, width, height, color, desired, assignedBucket):
        self.a = a
        self.b = b
        self.pos = (self.a, self.b)
        self.width = width
        self.height = height
        self.color = color
        self.selected = False
        self.attached = False
        self.IsMagnetized = False
        self.hit = (self.a, self.b,  self.width, self.height)
        self.desiredDir = desired
        self.assignedBucket = assignedBucket

    def draw(self, win):
        self.pos = (self.a,self.b)
        self.hit = (self.a, self.b , self.width, self.height)
        pygame.draw.rect(win, self.color, (self.a, self.b, self.width, self.height))  # Magnet change to win.blit(img,position) if wanting to put mag pic in

    def isOver(self, pos):
        # Pos is the mouse position or a tuple of (x,y) coordinates
        if pos[0] > self.a and pos[0] < self.a + self.width:
            if pos[1] > self.b and pos[1] < self.b + self.height:
                self.selected = True
                return True

        self.selected = False
        return False



class button():
    def __init__(self, color, x, y, width, height, text=''):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text

    def draw(self, win, outline=None):
        # Call this method to draw the button on the screen
        if outline:
            pygame.draw.rect(win, outline, (self.x - 2, self.y - 2, self.width + 4, self.height + 4), 0)

        pygame.draw.rect(win, self.color, (self.x, self.y, self.width, self.height), 0)

        if self.text != '':
            font = pygame.font.SysFont('comicsans', 20)
            text = font.render(self.text, 1, (0, 0, 0))
            win.blit(text, (self.x + (self.width / 2 - text.get_width() / 2),
                            self.y + (self.height / 2 - text.get_height() / 2)))

    def isOver(self, pos):
        # Pos is the mouse position or a tuple of (x,y) coordinates
        if pos[0] > self.x and pos[0] < self.x + self.width:
            if pos[1] > self.y and pos[1] < self.y + self.height:
                return True
        return False

class Bucket: # postion, desired direction for sorting "u" or "d", position = top or bottom
    def __init__(self, a, b, width, height, desired, color, position):
        self.a = a
        self.b = b
        self.width = width
        self.height = height
        self.pos = (self.a, self.b, self.a + self.width, self.b + self.height)
        self.hit = (self.a, self.b,self.width, self.height)
        self.desired = desired
        self.color = color
        self.position = position

        if position == "Top":
            self.acceptAdjust = 10
        elif position == "Bottom":
            self.acceptAdjust = -10  #need to accout for the height of the object


    def draw(self, win):
        pygame.draw.rect(win, self.color, (self.a, self.b, self.width, self.height))  # Bucket change to win.blit(img,position) if wanting to put mag pic in


class Path_Node:
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.local = 9999
        self.globe = 9999
        self.startingNode = False
        self.finishNode = False
        self.isVisited = False
        self.neigbours = []
        self.parent = None
    def __copy__(self):
        temp = Path_Node(self.a, self.b, self.local, self.globe, self.startingNode, self.finishNode, self.isVisited, self.neigbours, self.parent)
        return temp

#--------Functions ----------------------
def CreateObj(count):
    i = count % 4
    a = 50
    b = 50
    width = 80
    height = 40
    if i == 0: # red
        b =150
        color = (255,150,150)
        desired = "U"
    elif i == 1:
        b = 250
        color= (150,255,150)
        desired = "U"
    elif i==2:
        b = 350
        color = (150,150,255)
        desired= "D"
    elif i==3:
        b = 450
        color= (230, 165, 0)
        desired = "D"
    else:
        color = (50,50,50)

    return Object(a, b, width, height, color, desired, i)

def CreateObjCV(obj, table_pos ):
    print("Table Post: " + str(table_pos))
    a = obj.a*(table_pos[2] - table_pos[0])+ table_pos[0]
    b = obj.b *((table_pos[3]-table_pos[1])+ (2*table_pos[1]))
    width = obj.width * (table_pos[2] - table_pos[0])
    height = obj.height *((table_pos[3]-table_pos[1])+ (2*table_pos[1]))
    if a < table_pos[0]:
        a = table_pos[0]
    if a + width > table_pos[2]:
        a = table_pos[2] - width
    if b < table_pos[1]:
        b = table_pos[1]
    if b + height > table_pos[3]:
        b = table_pos[3] - height

    if obj.obj_class == 0: # red
        color = (255,150,150)
        desired = "U"
    elif obj.obj_class== 1:
        color= (150,255,150)
        desired = "U"
    elif obj.obj_class==2:
        color = (150,150,255)
        desired= "D"
    elif obj.obj_class==3:
        color= (255, 165, 0)
        desired = "D"
    else:
        color = (50,50,50)
        desired="U"

    return Object(a, b, width, height, color, desired, obj.obj_class)


def IsSelected(pos,list): #returns the ident of an object that is over a position
    i = 0
    for i in range(len(list)):
        if list[i].isOver(pos):
            return i
    return -1

def IsLocked(list):
    i=0
    for i in range(len(list)):
        if list[i].attached == True or list[i].IsMagnetized == True:
            return i

    return -1


def MoveObject(obj, pos):
    obj.a = pos[0] - obj.width/2
    obj.b = pos[1] - obj.height/2

def MoveObjectMag(obj,Mag,pos):
    obj.a = pos[0] - obj.width/2 + Mag.width/2
    obj.b = pos[1] - obj.height/2 + Mag.height/2

def MoveMagtoPos(win,table_pos, ObjectList, pos,Mag, Buckets): #Move magnet to position specified, used in GetObjwithMag
    movesa = abs(int((pos[0] - Mag.a)/Mag.vel))
    dira = pos[0] - Mag.a
    movesb = abs(int((pos[1] - Mag.b)/Mag.vel))
    dirb = pos[1] - Mag.b

    for i in range(movesa):
        if dira < 0 and Mag.a >= table_pos[0] + Mag.vel:
            Mag.a -= Mag.vel

        if dira >= 0 and Mag.a + Mag.width + Mag.vel <= table_pos[2]: #screeen width minus offset of table
            Mag.a += Mag.vel

        #--visulazation -----
        bg = pygame.image.load('../Simulator/img/Table1.jpg')
        win.blit(bg, (table_pos[0], table_pos[1]))
        for b in Buckets:
            b.draw(win)
        for o in ObjectList:
            o.draw(win)
        Mag.draw(win)
        pygame.display.update()
        #----------------------

    for j in range(movesb):
        if dirb < 0 and Mag.b >= Mag.vel:
                Mag.b -= Mag.vel
        if dirb >= 0 and Mag.b + Mag.height + Mag.vel <= table_pos[3] + Buckets[3].height: #screen height
                Mag.b += Mag.vel

        # --visulazation -----
        bg = pygame.image.load('../Simulator/img/Table1.jpg')
        win.blit(bg, (table_pos[0], table_pos[1]))
        for b in Buckets:
            b.draw(win)
        for o in ObjectList:
            o.draw(win)
        Mag.draw(win)
        pygame.display.update()
        # ----------------------

def MagnetizeObj(win,obj,Mag):
    if obj.isOver(Mag.pos) == True:
        if obj.IsMagnetized == True:
            obj.IsMagnetized = False
        else:
            obj.IsMagnetized = True


def Sort(Path, win, Buckets, obj, ObjectList, Mag):
    i = 0
    for i in range(len(Path)):
        dir = Path[i]
        if dir == "U":
            Mag.b -= Mag.vel

        if dir == "D":
            Mag.b += Mag.vel

        if dir == "R":
            Mag.a += Mag.vel

        if dir == "L":
            Mag.a -= Mag.vel

        MoveObjectMag(obj, Mag, Mag.pos)

        # --visulazation -----
        bg = pygame.image.load('../Simulator/img/Table1.jpg')
        win.blit(bg, (200, 104))
        for b in Buckets:
            b.draw(win)
        for o in ObjectList:
            o.draw(win)
            Mag.draw(win)

        pygame.display.update()
        # ----------------------
    print("After Sorting Obj Position: "+ str(obj.pos))
    print("After Sorting Ending Mag Pos" + str(Mag.pos))
    ObjectList.remove(obj)


def SortObjects(win, ObjectList, Mag, table_pos, Buckets, isAstar, isVisOn, table_controller):
    try:
        for o in ObjectList:
            if o.a < table_pos[0] or o.b < table_pos[1]:
                message_display(win, "Object not Reachable", times=1)
                return

        for obj in reversed(ObjectList):
            #-------
            MoveMagtoPos(win,table_pos, ObjectList, (obj.a + obj.width/2 - Mag.width / 2, obj.b + obj.height/2 - Mag.height / 2), Mag, Buckets)
            MagnetizeObj(win,obj,Mag)
            #------
            if not isAstar:
                PathFound = Algorithms.ModifiedDFS(win, ObjectList, Mag.vel, table_pos, Buckets, isVisOn)
            else:
                PathFound = Algorithms.AStar(win, ObjectList, Mag.vel, table_pos, Buckets, isVisOn)

            if not PathFound == "":
                print("Object Starting Position: " + str(obj.pos))
                print("Mag Starting Position: " + str(obj.pos))

                tempMagpos = Mag.pos
                Sort(PathFound, win, Buckets, obj, ObjectList, Mag)
                TableControl.sort_on_board(table_pos, Buckets, tempMagpos, Mag.vel , PathFound, table_controller)

            else:
                #ObjectList.insert(0,obj)
                message_display(win, "No Path Found", times=1)

        MoveMagtoPos(win, table_pos, ObjectList, (table_pos[0] + (table_pos[2] - table_pos[0])/2,(table_pos[3] - table_pos[1])/2), Mag, Buckets) #reset mag to start
    except:
        return

def text_objects(text, font):
    textSurface = font.render(text, True, (255,255,255))
    return textSurface, textSurface.get_rect()

def message_display(win, text, times =.001):
    largeText = pygame.font.Font('freesansbold.ttf',115)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((500,200))
    win.blit(TextSurf, TextRect)
    pygame.display.update()
    time.sleep(times)



