import queue
import pygame
import Objects
import time

import random


#Path Planning --------------

#-----Breadth First ------------
def valid(win, ObjList, obj_ident, InitialPosition, moves, vel, table_pos):
    i = InitialPosition[0]
    j = InitialPosition[1]
    for move in moves:

        if move == "L":
            i -= vel

        elif move == "R":
            i += vel

        elif move == "U":
            j -= vel

        elif move == "D":
            j += vel


        if  i < table_pos[0] or i + ObjList[obj_ident].width > table_pos[2]: #off the table horizontal
            #print("print off the table   HOR" + str(i) + " " + str(ObjList[obj_ident].width) +  str(table_pos))
            return False
        if j < table_pos[1] or j + ObjList[obj_ident].height > table_pos[3]: #off the table vertical
            #print("print off the table   VER" + str(i) + "  " + str(j) +  str(table_pos))
            return False

        for object in ObjList:
            if obj_ident == ObjList.index(object):
                continue #Same object
            ObsticaleRect = pygame.Rect(object.hit)
            MagObj = pygame.Rect(i,j, ObjList[obj_ident].width, ObjList[obj_ident].height)
            if MagObj.colliderect(ObsticaleRect)== True:
                #print(" collision detected")
                return False


    return True

def findEnd(win, ObjList, ident, InitialPosition, moves, vel,Bucket, isVisOn):

    i = InitialPosition[0]
    j = InitialPosition[1]

    for move in moves:
        if move == "L":
            i -= vel

        elif move == "R":
            i += vel

        elif move == "U":
            j -= vel

        elif move == "D":
            j += vel
    if isVisOn:
        bg = pygame.image.load('../Simulator/img/Table1.jpg') # visualization
        # algo visualization
        win.blit(bg, (200, 108))
        Bucket.draw(win)
        for o in ObjList:
            o.draw(win)

        pygame.draw.rect(win, (0, 0, 0), (i, j, ObjList[ident].width, ObjList[ident].height))
        pygame.display.update()

    # # ending position
    BucketRect = pygame.Rect(Bucket.a,Bucket.b + Bucket.acceptAdjust,Bucket.width - ObjList[ident].width,Bucket.height) # acceptAdjust helps get in right bucket
    MagObj = pygame.Rect(i, j, ObjList[ident].width, ObjList[ident].height)
    if MagObj.colliderect(BucketRect) == True:
        #print("Path Found: " + moves)
        return True
    return False


def ModifiedDFS(win,ObjList, vel, table_pos, Buckets, isVisOn): # must be locked on to an object
    obj_ident = Objects.IsLocked(ObjList)
    Bucket = Buckets[ObjList[obj_ident].assignedBucket]

    if not obj_ident == -1:
        obj = ObjList[obj_ident]
    else:
        return None

    path = queue.Queue()
    path.put("")
    add = ""
    desired = ObjList[obj_ident].desiredDir #desired direction for each block
    last = desired

    while not findEnd(win,ObjList,obj_ident, obj.hit, add, vel,Bucket, isVisOn):
        add = path.get()#dequing
        #print("This is path" + str(add)) #visualization

        put = add + desired
        if valid(win, ObjList, obj_ident, obj.hit, put, vel, table_pos): #desired direction eg Up or Down
            path.put(put)
            last = desired

        else:

            if last == desired:#if the last choice was up and it is blocked choose randomly left or right
                ran = random.randint(0,100)
                if ran % 2 == 0: #choose to go left =  0 or right = 1
                    choice="L"
                    put = add + choice
                    if valid(win,ObjList, obj_ident, obj.hit, put, vel, table_pos):
                        path.put(put)
                        last = choice

                else: # choose to go right
                    choice = "R"
                    put = add + choice
                    if valid(win,ObjList, obj_ident, obj.hit, put, vel, table_pos):
                        path.put(put)
                        last = choice

            else: #keep going left or right
                put = add + last
                if valid(win, ObjList, obj_ident, obj.hit, put, vel, table_pos):
                    path.put(put)

                else:  # cant go any farther left or right, turn around
                    if(last == "L"):
                        last = "R"
                        put = add + last
                        path.put(put)

                    elif (last == "R"):
                        last = "L"
                        put = add + last
                        path.put(put)

    return add


def createConnectedGraph(pos, tablepos, vel, Bucket, obj):

    poscounter = 0
    negcounter = 0
    StartGraphPos = [0, 0, 0, 0]

    position = pos[0]
    while position + vel <= tablepos[2]:
        position = position + vel
        poscounter += 1
    position = pos[0]
    while position - vel >= tablepos[0]:
        position = position - vel
        negcounter += 1

    starting_nodea = negcounter
    StartGraphPos[0] = pos[0] - (negcounter * vel)
    StartGraphPos[2] = pos[0] + (poscounter * vel)
    horNumberNodes = poscounter + negcounter

    poscounter = 0
    negcounter = 0
    position = pos[1]

    while position + vel <= tablepos[3] if Bucket.position == "Top" else position + vel <= tablepos[3] + abs(Bucket.pos[1] - Bucket.pos[3]) : #accounting for top and bottom buckets
        position = position + vel
        poscounter += 1
    position = pos[1]
    while position - vel >= tablepos[1] - abs(Bucket.pos[1] - Bucket.pos[3]) if Bucket.position == "Top" else position - vel >= tablepos[1]:
        position = position - vel
        negcounter += 1
    StartGraphPos[1] = pos[1] - (negcounter * vel)
    StartGraphPos[3] = pos[1] + (poscounter * vel)
    starting_nodeb = negcounter

    vertNumberNodes = poscounter + negcounter

    posa = StartGraphPos[0]
    posb = StartGraphPos[1]
    Graph = []
    nodeList =[]

    for i in range(horNumberNodes): #making the graph
        for j in range(vertNumberNodes):
            node = Objects.Path_Node(posa, posb)
            nodeList.append(node)
            posb += vel
        posa += vel
        posb = StartGraphPos[1]
        Graph.append(nodeList)
        nodeList = []

    StartNode = Graph[starting_nodea][starting_nodeb]
    StartNode.startingNode = True
    Finish_Nodes = []
    Real_World_offset = 20
    for i in range(len(Graph)): #assign parent and nodes
        for j in range(len(Graph[i])):
            if Graph[i][j].a > Bucket.pos[0] and Graph[i][j].a <  Bucket.pos[2] - obj.width:
                if (Graph[i][j].b >= Bucket.pos[1] and Graph[i][j].b <= Bucket.pos[3] - Bucket.height + vel) if Bucket.position == 'Top' else (Graph[i][j].b >= Bucket.pos[1] + Bucket.height - vel*2 and Graph[i][j].b <= Bucket.pos[3]): #Real World
                #if (Graph[i][j].b > Bucket.pos[1] and Graph[i][j].b <= Bucket.pos[3] - Bucket.height + vel ) if Bucket.position == 'Top' else (Graph[i][j].b + obj.height >= Bucket.pos[1] + Bucket.height - vel and Graph[i][j].b < Bucket.pos[3]):
                    #print("Node Posotion" + str(Graph[i][j].a) + " " + str(Graph[i][j].b) + " < " + str(Bucket.pos[0] + Bucket.pos[2]) + " " + str(Bucket.pos[1] + Bucket.pos[3]) )
                    Graph[i][j].finishNode = True
                    Finish_Nodes.append(Graph[i][j])
            Graph[i][j].neigbours = []
            if j > 0:
                Graph[i][j].neigbours.append(Graph[i][j-1])
            if j < vertNumberNodes-1:
                Graph[i][j].neigbours.append(Graph[i][j+1])
            if i > 0:
                Graph[i][j].neigbours.append(Graph[i-1][j])
            if i < horNumberNodes-1:
                Graph[i][j].neigbours.append(Graph[i+1][j])

    return Graph, StartNode, Finish_Nodes


def distance(a1,a2,b1,b2):
    dist = ((a1 - b1)**2 + (a2 - b2)**2)**.5
    return dist

def heuristic(a,b):
    return distance(a.a,a.b,b.a,b.b)

def getMinFinalNode(ptr, FinishingNodeList):
    min = 9999
    for i in FinishingNodeList:
        temp = heuristic(ptr,i)
        node = i
        if min > temp:
            min = temp
            bestNode = node
    return bestNode

def node_sort(node):
    return node.globe

def Astar_isValid(ObjList, obj_ident, vel, table_pos, p, Buckets):
    i = p.a
    j = p.b
    if i < table_pos[0] or i + ObjList[obj_ident].width > table_pos[2]:  # off the table horizontal
        #print("print off the table   HOR" + str(i) + " " + str(ObjList[obj_ident].width) +  str(table_pos))
        return False

    for object in ObjList:
        if obj_ident == ObjList.index(object):
            continue  # Same object
        ObsticaleRect = pygame.Rect(object.hit)
        MagObj = pygame.Rect(i, j, ObjList[obj_ident].width, ObjList[obj_ident].height)
        if MagObj.colliderect(ObsticaleRect) == True:
            #print(" collision detected----------------------------------")
            return False

    for bucket in Buckets:
        if bucket == Buckets[ObjList[obj_ident].assignedBucket]:
            continue
        ObsticaleRect = pygame.Rect(bucket.hit)
        MagObj = pygame.Rect(i, j, ObjList[obj_ident].width, ObjList[obj_ident].height)
        if MagObj.colliderect(ObsticaleRect) == True:
            return False

    return True

def getPathUsingParent(endNode, ObjList, obj_ident, vel, table_pos, Buckets):
    ptr = endNode
    moves = ""
    while ptr.startingNode == False:
        new_ptr = ptr.parent
        if new_ptr == None:
            break
        if Astar_isValid(ObjList, obj_ident, vel, table_pos, new_ptr, Buckets): #build the string backwards
            if new_ptr.a > ptr.a:
                moves = 'L' + moves
            if new_ptr.a < ptr.a:
                moves = 'R' + moves
            if new_ptr.b > ptr.b:
                moves = "U" + moves
            if new_ptr.b < ptr.b:
                moves = "D" + moves
        ptr = new_ptr

    return moves


def AStar(win,ObjList, vel, table_pos, Buckets, isVisOn):
    obj_ident = Objects.IsLocked(ObjList)
    Bucket = Buckets[ObjList[obj_ident].assignedBucket]
    obj = ObjList[obj_ident]


    if not obj_ident == -1:
        obj = ObjList[obj_ident]
    else:
        return None

    #parameterize the problem in a graph
    Graph, StartingNode, FinishNodes = createConnectedGraph(obj.pos, table_pos, vel, Bucket, obj)

    #initialization
    ptr = StartingNode
    StartingNode.local = 0
    FinNode = getMinFinalNode(StartingNode, FinishNodes)
    StartingNode.globe = heuristic(StartingNode,FinNode)
    NodesNotTested = [StartingNode]

    while len(NodesNotTested) != 0 and ptr.finishNode == False:
        NodesNotTested.sort(key=node_sort)
        while not len(NodesNotTested) == 0 and NodesNotTested[0].isVisited == True:
            NodesNotTested.pop(0)

        if len(NodesNotTested)==0:
            break
        ptr = NodesNotTested[0]
        ptr.isVisited= True

        for p in ptr.neigbours:
            if isVisOn:
                bg =pygame.image.load('../Simulator/img/Table1.jpg')  # visualization
                # algo visualization
                Bucket.draw(win)
                win.blit(bg, (table_pos[0], table_pos[1]))
                for o in ObjList:
                    o.draw(win)

                pygame.draw.rect(win, (50, 50, 50), (p.a, p.b, ObjList[obj_ident].width, ObjList[obj_ident].height))
                pygame.display.update()

            if not p.isVisited and Astar_isValid(ObjList, obj_ident, vel, table_pos, p, Buckets):
                NodesNotTested.append(p)


            possibleLowerGoal = ptr.local + heuristic(ptr, p)

            if possibleLowerGoal < p.local:
                p.parent = ptr
                p.local = possibleLowerGoal
                FinNode = getMinFinalNode(ptr, FinishNodes)
                p.globe = p.local + heuristic(p,FinNode)



    moves = getPathUsingParent(FinNode, ObjList, obj_ident, vel, table_pos, Buckets)

    #if isVisOn:
    pathobj = Objects.Object(StartingNode.a, StartingNode.b, 50, 50, (50,50,50), "U", 0)
    bg = pygame.image.load('../Simulator/img/Table1.jpg')  # visualization
    # algo visualization

    win.blit(bg, (table_pos[0], table_pos[1]))
    for o in ObjList:
        o.draw(win)
    for b in Buckets:
        b.draw(win)
    for m in moves:
        pygame.draw.rect(win, (50, 50, 50), (pathobj.a, pathobj.b, 10, 10))
        pygame.display.update()
        if m == "U":
            pathobj.b -= vel
        if m=="D":
            pathobj.b += vel
        if m == "L":
            pathobj.a -= vel
        if m == "R":
            pathobj.a +=vel
    time.sleep(2)

    return moves
