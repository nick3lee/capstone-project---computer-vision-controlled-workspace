import pygame
import Objects
import TableControl

def Organize(Object_init_list, table_controller):

    pygame.init()

    screen_width = 700
    screen_height =700
    win = pygame.display.set_mode((screen_width,screen_height))
    pygame.display.set_caption("Autonomous WorkBench")
    bg = pygame.image.load('../Simulator/img/Table1.jpg')
    clock = pygame.time.Clock()

    offset1 = 200 #longitude bg offset
    offset2 = 104  #latitude bg offset

    table_width = 435
    table_height = 492
    table_pos = (offset1,offset2,offset1+table_width, offset2 + table_height)

    #Init Buckets   #must specify either "Bottom" or "Top"
    Red_Bucket = Objects.Bucket(offset1, 0, table_width / 2, offset2,"U",(255,0,0),"Top") #red bucket
    Green_Bucket =Objects.Bucket(offset1 + table_width / 2, 0, table_width / 2, offset2, "U", (0,255,0), "Top") # green bucket
    Blue_Bucket = Objects.Bucket(offset1, table_height+ offset2, table_width / 2, offset2,"D",(0,0,255),"Bottom") # blue bucket
    White_Bucket = Objects.Bucket(offset1 + table_width / 2, table_height + offset2, table_width / 2, offset2, "D",(255,165,0), "Bottom") # white bucket
    Buckets = [Red_Bucket,Green_Bucket,Blue_Bucket,White_Bucket] #important order eg RED = 0, Green = 1, Blue = 2, White = 3, see CreateObj()


    #magnet
    width= 10
    height=10
    a=offset1 + table_width/2 - width/2
    b=offset2 + table_height/2 - height/2
    vel=3


    def WindowDraw():
        win.fill((0,0,0))
        win.blit(bg, (offset1, offset2))
        for bucket in Buckets:
            bucket.draw(win)


    def redrawGameWindow(): #add all objects to this window
        WindowDraw()
        Start_Button_DFS.draw(win,(0,0,0))
        Start_Button_A_Star.draw(win,(0,0,0))
        Add_Button.draw(win,(0,0,0))
        Visualize_Button.draw(win,(0,0,0))
        for o in ObjectList:
            o.draw(win)
        Mag.draw(win)

        pygame.display.update()

    def Quit():
        pygame.quit()


    #Init
    Mag = Objects.Magnet(a,b,width,height, vel)
    Start_Button_DFS = Objects.button((255,0,0),10,10,175,30, 'Depth First Organize')
    Start_Button_A_Star = Objects.button((200,0,100),10,40,175, 30, 'A* Organize')
    Visualize_Button = Objects.button((200, 0, 100), 10, 70, 175, 30, 'Visualization')
    Add_Button = Objects.button((0,0,255),10,100,175,30, 'Add_Object')

    # init
    obj_count = 0
    ObjectList = []
    obj_ident = -1
    isVisOn = False

    # init with 1 object
    #Object_init_list = []
    if len(Object_init_list) == 0:
        Obj = Objects.CreateObj(obj_count)
        obj_count += 1
        ObjectList.append(Obj)
    else:
        for i in Object_init_list:
            ObjectList.append(Objects.CreateObjCV(i,table_pos))
    
    
    #main Loop
    run = True
    while run:
        clock.tick(24)
        for event in pygame.event.get():
            pos = pygame.mouse.get_pos()

            if event.type == pygame.QUIT:
                run=False
        #mouse------------------------------------
            if event.type == pygame.MOUSEBUTTONDOWN:
                if Start_Button_DFS.isOver(pos):
                    print("Start")
                    isAstar = False
                    Objects.SortObjects(win,ObjectList,Mag,table_pos,Buckets, isAstar, isVisOn, table_controller)
                    run=False

                if Start_Button_A_Star.isOver(pos):
                    print("Start")
                    isAstar = True
                    Objects.SortObjects(win,ObjectList, Mag, table_pos, Buckets, isAstar, isVisOn, table_controller)
                    run=False

                if Visualize_Button.isOver(pos):
                    if not isVisOn:
                        isVisOn = True
                    else:
                        isVisOn = False
                if Add_Button.isOver(pos):
                    print("Add Object")
                    Obj = Objects.CreateObj(obj_count)
                    obj_count += 1
                    ObjectList.append(Obj)

                obj_ident = Objects.IsSelected(pos, ObjectList)  # is an object selected
                if not obj_ident == -1:
                    if ObjectList[obj_ident].isOver(pos): #lock the object to mouse
                        if ObjectList[obj_ident].attached == True:
                            ObjectList[obj_ident].attached = False
                        else:
                            ObjectList[obj_ident].attached = True


            if event.type == pygame.MOUSEMOTION:
                if Start_Button_DFS.isOver(pos):
                    Start_Button_DFS.color = (150,100,100)
                else:
                    Start_Button_DFS.color = (200,0,100)

                if Start_Button_A_Star.isOver(pos):
                    Start_Button_A_Star.color = (150,100,100)
                else:
                    Start_Button_A_Star.color = (200,0,100)

                if Visualize_Button.isOver(pos) or not isVisOn:
                    Visualize_Button.color = (200,0,100)

                elif Visualize_Button.isOver(pos) or isVisOn:
                    Visualize_Button.color = (150,100,100)

                if Add_Button.isOver(pos):
                    Add_Button.color = (100, 100, 150)
                else:
                    Add_Button.color = (50, 50, 255)

                obj_ident = Objects.IsLocked(ObjectList)  # is an objected locked
                if not obj_ident == -1:
                    if ObjectList[obj_ident].attached == True:
                        Objects.MoveObject(ObjectList[obj_ident],pos)


        #Magnet------------------------------
        obj_ident = Objects.IsSelected(Mag.pos, ObjectList)  # Is selected returns a int of object ident
        keys = pygame.key.get_pressed()
        if not obj_ident == -1: # grabbing object with magnet
            if keys[pygame.K_SPACE] and ObjectList[obj_ident].isOver(Mag.pos) == True:
                if ObjectList[obj_ident].IsMagnetized == True:
                    ObjectList[obj_ident].IsMagnetized = False
                else:
                    ObjectList[obj_ident].IsMagnetized = True
                    Objects.message_display(win, "Magnitized")

        if keys[pygame.K_UP] and Mag.b >= Mag.vel:
            Mag.b -= Mag.vel

        if keys[pygame.K_DOWN] and Mag.b + Mag.height + Mag.vel <= screen_height:
            Mag.b += Mag.vel

        if keys[pygame.K_LEFT] and Mag.a >= offset1 + Mag.vel:
            Mag.a -= Mag.vel

        if keys[pygame.K_RIGHT] and Mag.a + Mag.width + Mag.vel <= screen_width - offset1:
            Mag.a += Mag.vel

        obj_ident = Objects.IsLocked(ObjectList)
        if not obj_ident == -1: ##moving object with magnet
            if ObjectList[obj_ident].IsMagnetized == True:
                Objects.MoveObjectMag(ObjectList[obj_ident], Mag, Mag.pos)

        #collisions ------------------- logic
            # using obj_ident from the magnet islocked()
            for object in ObjectList:
                if obj_ident == ObjectList.index(object):
                    continue #Same object

                ObjRect = pygame.Rect(object.hit)
                MagObj = pygame.Rect(ObjectList[obj_ident].hit)
                if MagObj.colliderect(ObjRect)== True:
                    Objects.message_display(win,"Collision")

        redrawGameWindow()

    Quit()
    print("Done")

