#include <Queue.h>
#include <Servo.h>

#define limitSwitchXA1    7
#define limitSwitchXA2    10
#define limitSwitchXB1    6
#define limitSwitchXB2    A7
#define limitSwitchY1     11
#define limitSwitchY2     8

#define xStep     2
#define xDir      3
#define yStep     4
#define yDir      5

#define magServo  9

#define minCycle 750
#define maxCycle 900

int xSize = 2735;    //  In steps
int ySize = 1667;
int xDirection = -1;
int yDirection = -1;

int curPosition[] = { 0 , 0 };
int inputCoordinates[] = { 0 , 0 };
DataQueue<int> xCoordinates(40);
DataQueue<int> yCoordinates(40);
DataQueue<int> magnetQueue(40);
char travelParam;
int success = -1;
int magnetStatus = 1;

int initialization = -1;


Servo servo;

int xvalue = -1;
int yvalue = -1;
int value = -1;


void setup() {
  pinMode(limitSwitchXA1, INPUT);
  pinMode(limitSwitchXA2, INPUT);
  pinMode(limitSwitchXB1, INPUT);
  pinMode(limitSwitchXB2, INPUT);
  pinMode(limitSwitchY1, INPUT);
  pinMode(limitSwitchY2, INPUT);

  pinMode(xStep, OUTPUT);
  pinMode(xDir, OUTPUT);
  pinMode(yStep, OUTPUT);
  pinMode(yDir, OUTPUT);

  pinMode(magServo, OUTPUT);
  servo.attach(magServo);
  setMagValue(0);

  Serial.begin(9600);
  while (digitalRead(limitSwitchXA1)) {
    delay(100);
  }
  delay(2000);

  while (initialization == -1) {
    initialization =  calibrate();
  }

  Serial.print(initialization);
}


void loop() {
   if (Serial.available() > 0) {
    value = Serial.parseInt();
    // Set Magnet State -----
    if (value == -4){
      while(!(xCoordinates.isEmpty()) && !(yCoordinates.isEmpty())){
        setMagValue(magnetQueue.dequeue());
        inputCoordinates[0] = xCoordinates.dequeue();
        inputCoordinates[1] = yCoordinates.dequeue();
        diagonalMovement(inputCoordinates);
      }
      Serial.print(1);
    }
    if (value ==  -5) {
      Serial.print(value);
      magnetQueue.enqueue(1);
      //setMagValue(1); // turn on magnet
      value = -1;
    }
    else if ( value == -6) {
      Serial.print(value);
      magnetQueue.enqueue(0);
      //setMagValue(0);
      value = -1;
    }

    // Set Coordinates
    else if (xvalue == -1 && value >= 0 ) {
      xvalue = value;
      Serial.print(percentToStep(xvalue, xSize));
      Serial.flush();
      delay(100);
    }
    else if (yvalue == -1 && value >= 0) {
      yvalue = value;
      Serial.print(percentToStep(yvalue, ySize));
      Serial.flush();
      delay(100);
    }


    // If coordinates are initialized, move to position
    /*if (xvalue != -1 && yvalue != -1) {
      inputCoordinates[0] = percentToStep(xvalue, xSize);
      inputCoordinates[1] = percentToStep(yvalue, ySize);
      success  = diagonalMovement(inputCoordinates);
      Serial.print(success);

      xvalue = -1;
      yvalue = -1;
    }*/
    if (xvalue != -1 && yvalue != -1) {
      xCoordinates.enqueue(percentToStep(xvalue, xSize));
      yCoordinates.enqueue(percentToStep(yvalue, ySize));
      Serial.print(1);

      xvalue = -1;
      yvalue = -1;
    }
    value = -1;
  }

}


void setMagValue(int val) {
  /*int tempCoordsX = curPosition[0];
  int tempCoordsY = curPosition[1];
  int movement[] = {0,0};*/
  if (magnetStatus != val){
    if (val == 1) {
      servo.write(130);
      magnetStatus = 1;
      delay(1000);/*
      movement[0] = tempCoordsX - 100;
      movement[1] = tempCoordsY - 100;
      manhattanMovement(movement);
      movement[0] = tempCoordsX + 100;
      movement[1] = tempCoordsY + 100;
      manhattanMovement(movement);
      movement[0] = tempCoordsX;
      movement[1] = tempCoordsY;
      manhattanMovement(movement);*/
    }
    else {
      servo.write(0);
      magnetStatus = 0;
      delay(1000);
    }
  }
  //delay(1000);
}


bool calibrate() {
  int stepCountY;
  int stepCountX;

  changeDir('x', 1);
  xDirection = 1;
  changeDir('y', 1);
  yDirection = 1;
  while (digitalRead(limitSwitchY2)) {
    stepY(750);
  }
  while (digitalRead(limitSwitchXA1)) {
    stepX(750);
  }


  stepCountY = 0;
  stepCountX = 0;

  changeDir('x', -1);
  xDirection = -1;
  changeDir('y', -1);
  yDirection = -1;
  while (digitalRead(limitSwitchXA2)) {
    stepX(750);
    stepCountX++;
  }
  while (digitalRead(limitSwitchY1)) {
    stepY(750);
    stepCountY++;
  }

  xSize = stepCountX - 40;
  ySize = stepCountY - 40;


  changeDir('x', 1);
  xDirection = 1;
  changeDir('y', 1);
  yDirection = 1;

  for (int i = 0; i < 20; i++) {
    stepY(750);
  }
  for (int i = 0; i < 20; i++) {
    stepX(750);
  }

  curPosition[0] = 0;
  curPosition[1] = 0;


  return true;

}


void stepX(int delay) {
  digitalWrite(xStep, HIGH);
  delayMicroseconds(delay);
  digitalWrite(xStep, LOW);
  delayMicroseconds(delay);
  curPosition[0] += xDirection;
  return;
}


void stepY(int delay) {
  digitalWrite(yStep, HIGH);
  delayMicroseconds(delay);
  digitalWrite(yStep, LOW);
  delayMicroseconds(delay);
  curPosition[1] += yDirection;
  return;
}

int percentToStep(int percent, int stepVal) {
  return (int)(((float)percent * (float)stepVal) / 1000);
}


int manhattanMovement(int coordinates[]) {
  int tracker;
  bool xCorrectPos = false;
  bool yCorrectPos = false;
  int xRelativePos = abs(coordinates[0] - curPosition[0]);				//modifications to allow for manhattan acceleration and deceleration
  int yRelativePos = abs(coordinates[1] - curPosition[1]);
  int accelStepsX = xRelativePos*0.2;
  int accelStepsY = yRelativePos*0.2;
  int accelSteps[] = {accelStepsX, accelStepsY};
  int delayTime;
  int interval = (maxCycle - minCycle)/accelSteps[0]; 

  determineDirections(coordinates, &xCorrectPos, &yCorrectPos);

  //-----------------------------------
  // Travel to Positions:
  //-----------------------------------
  int i = 0;
  while (!xCorrectPos) {
    if (i>(xRelativePos-accelSteps[0])) {
      delayTime = minCycle + (interval*(i-(xRelativePos-accelSteps[0])));
      }
    else if (i<accelSteps[0]) {
      delayTime = maxCycle - (i*interval);
      }
    else {
      delayTime = minCycle;
      }
    tracker = 8;
    stepX(delayTime);
    if (!xLimitCheck()) {
      tracker = 2;
      break; // Sanity check. ToDo: error handling.
    }
    if (curPosition[0] == coordinates[0]) {
      xCorrectPos = true;
      tracker = 3;
      break;
    }
    i++;
  }
  i = 0;
  while (!yCorrectPos) {
    if (i>(yRelativePos-accelSteps[1])) {
      delayTime = minCycle + (interval*(i-(yRelativePos-accelSteps[1])));
      }
    else if (i<accelSteps[1]) {
      delayTime = maxCycle - (i*interval);
      }
    else {
      delayTime = minCycle;
      }
    stepY(delayTime);
    if (!yLimitCheck()) {
      break; // Sanity check. ToDo: error handling.
    }
    if (curPosition[1] == coordinates[1]) {
      yCorrectPos = true;
      break;
    }
    i++;
  }
  //-----------------------------------
  // Check for successful movement
  //-----------------------------------
  if (xCorrectPos == true && yCorrectPos == true) {
    return 1;
  }
  else {
    return tracker;
  }
}


void determineDirections(int coordinates[], bool *xCorrectPos, bool *yCorrectPos) {
  int xRelativePos = coordinates[0] - curPosition[0];
  int yRelativePos = coordinates[1] - curPosition[1];

  if (xRelativePos > 0) {
    changeDir('x', 1);
    xDirection = 1;
  }
  else if (xRelativePos == 0) {
    *xCorrectPos = true;
  }
  else {
    changeDir('x', -1);
    xDirection = -1;
  }

  if (yRelativePos > 0) {
    changeDir('y', 1);
    yDirection = 1;
  }
  else if (yRelativePos == 0) {
    *yCorrectPos = true;
  }
  else {
    changeDir('y', -1);
    yDirection = -1;
  }
}


bool xLimitCheck() {
  bool result = true;
  if (!digitalRead(limitSwitchXA1) || !digitalRead(limitSwitchXA2)) {
    if (xDirection == 1) {
      xDirection = -1;
      changeDir('x', -1);
      for (int x = 0; x < 20; x++) {
        stepX(750);
      }
      curPosition[0] = xSize;
    }
    else {
      xDirection = 1;
      changeDir('x', 1);
      for (int x = 0; x < 20; x++) {
        stepX(750);
      }
      curPosition[0] = 0;
    }

    result = false;
  }
  return result;
}


bool yLimitCheck() {
  bool result = true;
  if (!digitalRead(limitSwitchY1) || !digitalRead(limitSwitchY2)) {
    if (yDirection == 1) {
      yDirection = -1;
      changeDir('y', -1);
      for (int x = 0; x < 20; x++) {
        stepY(750);
      }
      curPosition[1] = ySize;
    }
    else {
      yDirection = 1;
      changeDir('y', 1);
      for (int x = 0; x < 20; x++) {
        stepY(750);
      }
      curPosition[1] = 0;
    }
    result = false;
  }
  return result;
}


int diagonalMovement(int coordinates[]) {
  int return_code = 0;
  bool xCorrectPos = false;
  bool yCorrectPos = false;
  int xRelativePos = abs(coordinates[0] - curPosition[0]);
  int yRelativePos = abs(coordinates[1] - curPosition[1]);
 /* int accelStepsX = xRelativePos*0.2;
  int accelStepsY = yRelativePos*0.2;
  */
  int accelSteps[] = {0, 0};
  int delayTime;
  double threshold;
  int interval;


  determineDirections(coordinates, &xCorrectPos, &yCorrectPos);

  //Case 1: x relative pos = y relative pos = 0
  if ((xRelativePos == 0) && (yRelativePos == 0)) {
    return_code = 1;
  }

  //Case 2: x relative pos = 0
  else if (xRelativePos == 0) {
    manhattanMovement(coordinates);
    return_code = 2;
  }

  //Case 3: y relative pos = 0
  else if (yRelativePos == 0) {
    manhattanMovement(coordinates);
    return_code = 3;
  }

  //Case 4: x relative pos = x relative pos
  else {//if (abs(xRelativePos - yRelativePos)<100) {
    threshold = xRelativePos*(xRelativePos<=yRelativePos) + yRelativePos*(xRelativePos>yRelativePos);
	  accelSteps[0] = threshold*0.2;
    interval = (maxCycle - minCycle)/accelSteps[0];
    for (int i=0; i<threshold; i++) {
      if (i>(threshold-accelSteps[0])) {
        delayTime = minCycle + (interval*(i-(threshold-accelSteps[0])));
        }
      else if (i<accelSteps[0]) {
        delayTime = maxCycle - (i*interval);
        }
      else {
        delayTime = minCycle;
        }
      digitalWrite(xStep, HIGH);
      digitalWrite(yStep, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(xStep, LOW);
      digitalWrite(yStep, LOW);
      delayMicroseconds(delayTime);
      curPosition[0] += xDirection;
      curPosition[1] += yDirection;
    }
    manhattanMovement(coordinates);
    return_code = 4;
  }

  //Case 5: x relative pos >< y relative pos
  /*
  else {
    return_code = 5;
    int xRelativePos5 = round200(xRelativePos);
    int yRelativePos5 = round200(yRelativePos);
    int delayTime, gcd, xIncrement, yIncrement, xStatusTracker, yStatusTracker;

    if ((xRelativePos5 - yRelativePos5) > 0) {
      gcd = greatestCommonDivisor(xRelativePos5, yRelativePos5);

    }
    else {
      gcd = greatestCommonDivisor(yRelativePos5, xRelativePos5);

    }

    yIncrement = xRelativePos5 / gcd;
    xIncrement = yRelativePos5 / gcd;

    if (xIncrement < yIncrement) {
      threshold = (double)xIncrement * (double)xRelativePos5 * 2;
      delayTime = 500 / xIncrement;
    }
    else {
      threshold = (double)yIncrement * (double)yRelativePos5 * 2;
      delayTime = 500 / yIncrement;
    }
    if (delayTime == 0) {
      delayTime = 1;
    }

    for (double j = 0; j < threshold; j++) {
      if ((fmod(j, xIncrement)) == 0) {
        xStatusTracker += 1;
        xStatusTracker = xStatusTracker % 2;
        digitalWrite(xStep, xStatusTracker);
        if (xStatusTracker == 0) {
          curPosition[0] += xDirection;
        }
      }
      if ((fmod(j, yIncrement)) == 0) {
        yStatusTracker += 1;
        yStatusTracker = yStatusTracker % 2;
        digitalWrite(yStep, yStatusTracker);
        if (yStatusTracker == 0) {
          curPosition[1] += yDirection;
        }
      }
      delayMicroseconds(delayTime);
      //      if (xLimitCheck() || yLimitCheck()){
      //        return_code == -1;
      //        break;
      //      }
    }
    manhattanMovement(coordinates);
  }*/
  return return_code;
}



unsigned greatestCommonDivisor(unsigned m, unsigned n)
{
  if (n == 0) return m;
  return greatestCommonDivisor(n, m % n);
}


int round200(int n) {
  return (n / 200) * 200;
}


void changeDir(char axis, int dir){
  if (axis == 'x'){
    if (dir == -1){
      digitalWrite(xDir, HIGH);
    }
    else if(dir == 1){
      digitalWrite(xDir, LOW);
    }
  }
  else if(axis == 'y'){
    if (dir == 1){
      digitalWrite(yDir, HIGH);
    }
    else if(dir == -1){
      digitalWrite(yDir, LOW);
    }
  }
}