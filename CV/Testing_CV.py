import cv2 as cv
import numpy as np

iCannyLowerThreshold = 25
iCannyUpperThreshold = 3*iCannyLowerThreshold
cap = cv.VideoCapture(0)


init = 'Initialization'
rect_wl_name = 'Width L'
rect_ht_name = 'Height T'
rect_wr_name = 'Width R'
rect_hb_name = 'Height B'

rect_wl = 310
rect_ht = 178
rect_wr = 310
rect_hb = 178
x_scale = 0
y_scale = 0
x_shift = 0
y_shift = 0


def ht_trackbar(val):
    global rect_ht
    rect_ht = val
    cv.setTrackbarPos(rect_ht_name, init, rect_ht)

def wl_trackbar(val):
    global rect_wl
    rect_wl = val
    cv.setTrackbarPos(rect_wl_name, init, rect_wl)
def hb_trackbar(val):
    global rect_hb
    rect_hb = val
    cv.setTrackbarPos(rect_hb_name, init, rect_hb)

def wr_trackbar(val):
    global rect_wr
    rect_wr = val
    cv.setTrackbarPos(rect_wr_name, init, rect_wr)


w_max_value = int(cap.get(cv.CAP_PROP_FRAME_WIDTH)/2)
h_max_value = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)/2)
cv.namedWindow(init)
cv.resizeWindow(init,512,200)
cv.createTrackbar(rect_ht_name, init, rect_ht, h_max_value, ht_trackbar)
cv.createTrackbar(rect_wl_name, init, rect_wl, w_max_value, wl_trackbar)
cv.createTrackbar(rect_hb_name, init, rect_hb, h_max_value, hb_trackbar)
cv.createTrackbar(rect_wr_name, init, rect_wr, w_max_value, wr_trackbar)

while True:

    ret, frame = cap.read()
    if frame is None:
        break

    frame_rows,frame_cols = frame.shape[:2]
    p1 = (int((frame_cols/2 )- rect_wl), int((frame_rows/2)-rect_ht))
    p2 = (int((frame_cols / 2) + rect_wr), int((frame_rows / 2) + rect_hb))

    cv.rectangle(frame, p1, p2, (0, 0,0), 1)
    cv.imshow("Select Table Insides",frame)
    cv.imshow(init,np.zeros([1,1,3],dtype = np.uint8))
    key = cv.waitKey(50)
    if key == ord(' '):
        roi = frame[p1[1] + 1:p2[1] - 1, p1[0] + 1:p2[0] - 1]
        cv.imshow("ROI", roi)
        key = cv.waitKey()
        if key == ord('r'):
            continue
        else:
            x_shift = p1[0]
            y_shift = p1[1]
            x_scale = (p2[0] - p1[0])
            y_scale = (p2[1] - p1[1])
            table_rect= (p1,p2)
            # hsvRoi = cv.cvtColor(roi, cv.COLOR_BGR2HSV)
            # lower = np.array([hsvRoi[:, :, 0].min(), hsvRoi[:, :, 1].min(), hsvRoi[:, :, 2].min()])
            # upper = np.array([hsvRoi[:, :, 0].max(), hsvRoi[:, :, 1].max(), hsvRoi[:, :, 2].max()])
            break
    cv.destroyWindow("ROI")
    key = cv.waitKey(30)
    if key == ord('q') or key == 27:
        exit(-1)
cv.destroyAllWindows()

while True:

    ret, frame = cap.read()
    if frame is None:
        break
    img = frame.copy()
    cv.rectangle(img,table_rect[0],table_rect[1],(0, 0, 0), 1)
    cv.imshow("Window", img)
    cv.waitKey()
#

#
# while True:
#
#     ret, frame = cap.read()
#     if frame is None:
#         break
#     img = frame.copy()
#     gray= cv.cvtColor(img, cv.COLOR_BGR2GRAY)
#     mask = gray.copy()
#     ret_val, mask = cv.threshold(mask,150,255,cv.THRESH_BINARY)
# #Harris corner
#     # dst = cv.cornerHarris(gray, 2, 3, 0.04)
#     #
#     # # result is dilated for marking the corners, not important
#     # dst = cv.dilate(dst, None)
#     # # cv.imshow("hello", dst)
#     # # cv.waitKey()
#     # mask[dst > 0.01 * dst.max()] = 255
#     # mask[mask<255] = 0
#     #
#     # # Threshold for an optimal value, it may vary depending on the image.
#     # img[dst > 0.01 * dst.max()] = [0, 0, 255]
#     cv.imshow("hello", mask)
#     cv.waitKey()
#     contours, hierarchy = cv.findContours(mask, cv.RETR_EXTERNAL,cv.CHAIN_APPROX_SIMPLE)
#
#     # rows= img.shape[0]
#     # cols= img.shape[1]
#     # y = int(rows/4)
#     # h = int(y*2)
#     # x = int(cols / 4)
#     # w = int(x * 2)
#     #
#     # mask = np.zeros(img.shape, np.uint8)
#     # mask[y:y + h, x:x + w] = img[y:y + h, x:x + w]
#     # # ret, img = cv.threshold(img, 30, 255, cv.THRESH_BINARY_INV)
#     # img =  cv.GaussianBlur(mask,(5,5),0)
#     # img = cv.Canny(img, iCannyLowerThreshold, iCannyUpperThreshold);
#     # cv.imshow("Canny", img)
#     # cv.waitKey()
#     # contours, hierarchy = cv.findContours(img, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)
#     #
#     #
#     # print(hierarchy)
#     #  #Cycle through contours and add area to array
#     # areas = []
#     # square=[]
#     # for c in contours:
#     #     areas.append(cv.contourArea(c))
#     #     peri = cv.arcLength(c, True)
#     #     approx = cv.approxPolyDP(c, 0.04 * peri, True)
#     #     #print(approx)
#     #     # if approx == 4:
#     #     #     square.append(c)
#
#     # Sort array of areas by size
#     # sorted_areas = sorted(zip(areas, contours), key=lambda x: x[0], reverse=True)
#     cv.drawContours(img,contours,-1,(255,0,0),4)
#     cv.imshow("Window", img)
#     cv.waitKey()
#
