
import cv2 as cv
import numpy as np
import Simulator
import TableControl

print("Hit the Calibrate")
table_controller = TableControl.TableControl()
cap = cv.VideoCapture(1)

######################### INITIALIZATION ##############################
init = 'Initialization'
rect_wl_name = 'Width L'
rect_ht_name = 'Height T'
rect_wr_name = 'Width R'
rect_hb_name = 'Height B'

rect_wl = 311
rect_ht = 172
rect_wr = 303
rect_hb = 212
x_scale = 0
y_scale = 0
x_shift = 0
y_shift = 0


def ht_trackbar(val):
    global rect_ht
    rect_ht = val
    cv.setTrackbarPos(rect_ht_name, init, rect_ht)

def wl_trackbar(val):
    global rect_wl
    rect_wl = val
    cv.setTrackbarPos(rect_wl_name, init, rect_wl)
def hb_trackbar(val):
    global rect_hb
    rect_hb = val
    cv.setTrackbarPos(rect_hb_name, init, rect_hb)

def wr_trackbar(val):
    global rect_wr
    rect_wr = val
    cv.setTrackbarPos(rect_wr_name, init, rect_wr)


w_max_value = int(cap.get(cv.CAP_PROP_FRAME_WIDTH)/2)
h_max_value = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)/2)
cv.namedWindow(init)
cv.resizeWindow(init,512,200)
cv.createTrackbar(rect_ht_name, init, rect_ht, h_max_value, ht_trackbar)
cv.createTrackbar(rect_wl_name, init, rect_wl, w_max_value, wl_trackbar)
cv.createTrackbar(rect_hb_name, init, rect_hb, h_max_value, hb_trackbar)
cv.createTrackbar(rect_wr_name, init, rect_wr, w_max_value, wr_trackbar)




while True:

    ret, frame = cap.read()
    if frame is None:
        break

    frame_rows,frame_cols = frame.shape[:2]
    p1 = (int((frame_cols/2 )- rect_wl), int((frame_rows/2)-rect_ht))
    p2 = (int((frame_cols / 2) + rect_wr), int((frame_rows / 2) + rect_hb))

    cv.rectangle(frame, p1, p2, (0, 0,0), 1)
    cv.imshow("Select Table Insides",frame)
    cv.imshow(init,np.zeros([1,1,3],dtype = np.uint8))
    key = cv.waitKey(50)
    if key == ord(' '):
        roi = frame[p1[1] + 1:p2[1] - 1, p1[0] + 1:p2[0] - 1]
        cv.imshow("ROI", roi)
        key = cv.waitKey()
        if key == ord('r'):
            continue
        else:
            # x_shift = p1[0]
            # y_shift = p1[1]
            # x_scale = (p2[0] - p1[0])
            # y_scale = (p2[1] - p1[1])
            table_top = p1[1]
            table_left = p1[0]
            table_bottom = p2[1]
            table_right = p2[0]


            break
    cv.destroyWindow("ROI")
    key = cv.waitKey(30)
    if key == ord('q') or key == 27:
        exit(-1)
cv.destroyAllWindows()

####### END INITALIZATION #######################################
x_sim_shift = 201
y_sim_shift = 0
objects = []


class CV_Object:
    def __init__(self, a, b, width, height, obj_class):
        self.a = a
        self.b = b
        self.width = width
        self.height = height
        self.obj_class =obj_class #0-3


def remove_collision(class_rect):
    if (len(class_rect) > 1):
        for r1 in class_rect:
            rect1x=r1.a
            rect1y=r1.b
            rect1width= r1.width
            rect1height = r1.height
            for r2 in class_rect:
                rect2x = r2.a
                rect2y = r2.b
                rect2width = r2.width
                rect2height = r2.height

                if (rect1x < rect2x + rect2width and
                    rect1x + rect1width > rect2x and
                    rect1y < rect2y + rect2height and
                    rect1y + rect1height > rect2y and
                    r1!=r2):
                    class_rect.remove(r2)
    return class_rect

# Initialize the parameters
confThreshold = 0.4  #Confidence threshold
nmsThreshold = 0.4   #Non-maximum suppression threshold
inpWidth = 416       #Width of network's input image
inpHeight = 416      #Height of network's input image
# Load names of classes
classesFile = "coco.names";
classes = None
with open(classesFile, 'rt') as f:
    classes = f.read().rstrip('\n').split('\n')

# Give the configuration and weight files for the model and load the network using them.
modelConfiguration = "yolov3.cfg";
modelWeights = "yolov3.weights";

net = cv.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)


# Get the names of the output layers
def getOutputsNames(net):
    # Get the names of all the layers in the network
    layersNames = net.getLayerNames()
    # Get the names of the output layers, i.e. the layers with unconnected outputs
    return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]

# Draw the predicted bounding box
def drawPred(classId, conf, left, top, right, bottom):
    # Draw a bounding box.
    cv.rectangle(frame, (left, top), (right, bottom), (255, 178, 50), 3)
    
    label = '%.2f' % conf
        
    # Get the label for the class name and its confidence
    if classes:
        assert(classId < len(classes))
        label = '%s:%s' % (classes[classId], label)

    #Display the label at the top of the bounding box
    labelSize, baseLine = cv.getTextSize(label, cv.FONT_HERSHEY_SIMPLEX, 0.5, 1)
    top = max(top, labelSize[1])
    cv.rectangle(frame, (left, top - round(1.5*labelSize[1])), (left + round(1.5*labelSize[0]), top + baseLine), (255, 255, 255), cv.FILLED)
    cv.putText(frame, label, (left, top), cv.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,0), 1)

# Remove the bounding boxes with low confidence using non-maxima suppression
def postprocess(frame, outs):
    frameHeight = frame.shape[0]
    frameWidth = frame.shape[1]
    objects=[]

    # Scan through all the bounding boxes output from the network and keep only the
    # ones with high confidence scores. Assign the box's class label as the class with the highest score.
    classIds = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > confThreshold:
                center_x = int(detection[0] * frameWidth)
                center_y = int(detection[1] * frameHeight)
                width = int(detection[2] * frameWidth)
                height = int(detection[3] * frameHeight)
                left = int(center_x - width / 2)
                top = int(center_y - height / 2)
                classIds.append(classId)
                confidences.append(float(confidence))
                boxes.append([left, top, width, height])

    # Perform non maximum suppression to eliminate redundant overlapping boxes with
    # lower confidences.
    indices = cv.dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThreshold)
    for i in indices:
        i = i[0]
        box = boxes[i]
        left = box[0]
        top = box[1]
        width = box[2]
        height = box[3]
        drawPred(classIds[i], confidences[i], left, top, left + width, top + height)
        # x = left/frameWidth
        # y = top/frameHeight
        # width = width/frameWidth
        # height = height/frameHeight
        # objects.append(CV_Object(x, y, width, height, (classIds[i] % 3)))
        # Switching coordinates
        x_new = (frameHeight - (top+height))/frameHeight
        y_new = left/frameWidth
        width_new = height/frameHeight
        height_new = width/frameWidth
        objects.append(CV_Object(x_new,y_new,width_new,height_new, (classIds[i]%3)))
    objects = remove_collision(objects)
    return objects




########################################################################################
# Webcam input

while cv.waitKey(1) < 0:
    
    # get frame from the video
    hasFrame, frame = cap.read()

    # Stop the program if reached end of video
    if not hasFrame:
        print("Error")
        cv.waitKey(3000)
        # Release device
        cap.release()
        break
    frame = frame[table_top:table_bottom, table_left:table_right]
    # Create a 4D blob from a frame.
    blob = cv.dnn.blobFromImage(frame, 1/255, (inpWidth, inpHeight), [0,0,0], 1, crop=False)

    # Sets the input to the network
    net.setInput(blob)

    # Runs the forward pass to get output of the output layers
    outs = net.forward(getOutputsNames(net))

    # Remove the bounding boxes with low confidence
    objects = postprocess(frame, outs)

    # Put efficiency information. The function getPerfProfile returns the overall time for inference(t) and the timings for each of the layers(in layersTimes)
    t, _ = net.getPerfProfile()
    label = 'Inference time: %.2f ms' % (t * 1000.0 / cv.getTickFrequency())
    cv.putText(frame, label, (0, 15), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))


    cv.imshow("Detection", frame)
    cv.waitKey(1500)

    Simulator.Organize(objects, table_controller)
    objects.clear()

