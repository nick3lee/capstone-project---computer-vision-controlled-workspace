import cv2 as cv
import numpy as np
import Simulator
import TableControl

print("Hit the Calibrate")
table_controller = TableControl.TableControl()
cap = cv.VideoCapture(1)

######################### INITIALIZATION ##############################
init = 'Initialization'
rect_wl_name = 'Width L'
rect_ht_name = 'Height T'
rect_wr_name = 'Width R'
rect_hb_name = 'Height B'

rect_wl = 311
rect_ht = 172
rect_wr = 303
rect_hb = 212
x_scale = 0
y_scale = 0
x_shift = 0
y_shift = 0


def ht_trackbar(val):
    global rect_ht
    rect_ht = val
    cv.setTrackbarPos(rect_ht_name, init, rect_ht)

def wl_trackbar(val):
    global rect_wl
    rect_wl = val
    cv.setTrackbarPos(rect_wl_name, init, rect_wl)
def hb_trackbar(val):
    global rect_hb
    rect_hb = val
    cv.setTrackbarPos(rect_hb_name, init, rect_hb)

def wr_trackbar(val):
    global rect_wr
    rect_wr = val
    cv.setTrackbarPos(rect_wr_name, init, rect_wr)


w_max_value = int(cap.get(cv.CAP_PROP_FRAME_WIDTH)/2)
h_max_value = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)/2)
cv.namedWindow(init)
cv.resizeWindow(init,512,200)
cv.createTrackbar(rect_ht_name, init, rect_ht, h_max_value, ht_trackbar)
cv.createTrackbar(rect_wl_name, init, rect_wl, w_max_value, wl_trackbar)
cv.createTrackbar(rect_hb_name, init, rect_hb, h_max_value, hb_trackbar)
cv.createTrackbar(rect_wr_name, init, rect_wr, w_max_value, wr_trackbar)


while True:

    ret, frame = cap.read()
    if frame is None:
        break

    frame_rows,frame_cols = frame.shape[:2]
    p1 = (int((frame_cols/2 )- rect_wl), int((frame_rows/2)-rect_ht))
    p2 = (int((frame_cols / 2) + rect_wr), int((frame_rows / 2) + rect_hb))

    cv.rectangle(frame, p1, p2, (0, 0,0), 1)
    cv.imshow("Select Table Insides",frame)
    cv.imshow(init,np.zeros([1,1,3],dtype = np.uint8))
    key = cv.waitKey(50)
    if key == ord(' '):
        roi = frame[p1[1] + 1:p2[1] - 1, p1[0] + 1:p2[0] - 1]
        cv.imshow("ROI", roi)
        key = cv.waitKey()
        if key == ord('r'):
            continue
        else:

            table_top = p1[1]
            table_left = p1[0]
            table_bottom = p2[1]
            table_right = p2[0]


            break
    cv.destroyWindow("ROI")
    key = cv.waitKey(30)
    if key == ord('q') or key == 27:
        exit(-1)
cv.destroyAllWindows()


max_value = 512
init = 'Initialization'
rect_w_name = 'Width'
rect_h_name = 'Height'
rect_w = 25
rect_h = 25
thresh = []

#parameters
num_objects = 6
num_classes = 2
x_scale = 2
y_scale=2
min_area = 500


class CV_Object:
    def __init__(self, a, b, width, height, obj_class):
        self.a = a
        self.b = b
        self.width = width
        self.height = height
        self.obj_class =obj_class #0-3

def remove_collision(class_rect):
    if (len(class_rect) > 1):
        for r1 in class_rect:
            rect1x=r1.a
            rect1y=r1.b
            rect1width= r1.width
            rect1height = r1.height
            for r2 in class_rect:
                rect2x = r2.a
                rect2y = r2.b
                rect2width = r2.width
                rect2height = r2.height

                if (rect1x < rect2x + rect2width and
                    rect1x + rect1width > rect2x and
                    rect1y < rect2y + rect2height and
                    rect1y + rect1height > rect2y and
                    r1!=r2):
                    class_rect.remove(r2)
    return class_rect

def Draw_Class(img,rect,color):
    for i in range(0,len(rect)):
        cv.rectangle(img, (rect[i][0], rect[i][1]),
                     (rect[i][0] + rect[i][2], rect[i][1] + rect[i][3]), color, 1)

def Find_Classes(img,lower,upper):
    class_rect = []
    global min_area
    kernel = np.ones((3, 3), np.uint8)
    img = cv.inRange(img, lower, upper)
    img = cv.morphologyEx(img, cv.MORPH_OPEN, kernel)
    img= cv.morphologyEx(img, cv.MORPH_CLOSE, kernel)
    mask = img
    class_cnt, hierarchy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    class_cnt = sorted(class_cnt, key=cv.contourArea, reverse=True)[:num_objects]
    class_rect.clear()
    for i in range(0, len(class_cnt)):
        if cv.contourArea(class_cnt[i])> min_area:
            class_rect.append(cv.boundingRect(class_cnt[i]))

    return img,class_rect

def h_trackbar(val):
    global rect_h
    rect_h = val
    cv.setTrackbarPos(rect_h_name, init, rect_h)

def w_trackbar(val):
    global rect_w
    rect_w = val
    cv.setTrackbarPos(rect_w_name, init, rect_w)

cv.namedWindow(init)
cv.createTrackbar(rect_h_name, init, rect_h, max_value, h_trackbar)
cv.createTrackbar(rect_w_name, init, rect_w, max_value, w_trackbar)

while True:

    ret, frame = cap.read()
    if frame is None:
        break

    frame_rows,frame_cols = frame.shape[:2]
    p1 = (int((frame_cols/2 )- rect_w), int((frame_rows/2) -rect_h))
    p2 = (int((frame_cols / 2) + rect_w), int((frame_rows / 2) + rect_h))
    cv.rectangle(frame, p1, p2, (255, 0,0), 1)
    cv.imshow(init, frame)
    key = cv.waitKey(50)
    if key == ord(' '):
        roi = frame[p1[1] + 1:p2[1] - 1, p1[0] + 1:p2[0] - 1]
        cv.imshow("ROI", roi)
        key = cv.waitKey()
        if key == ord('r'):
            continue

        else:
            hsvRoi = cv.cvtColor(roi, cv.COLOR_BGR2HSV)
            lower = np.array([hsvRoi[:, :, 0].min(), hsvRoi[:, :, 1].min(), hsvRoi[:, :, 2].min()])
            upper = np.array([hsvRoi[:, :, 0].max(), hsvRoi[:, :, 1].max(), hsvRoi[:, :, 2].max()])
            print(lower)
            print(upper)

            thresh.append((lower,upper))
    cv.destroyWindow("ROI")

    key = cv.waitKey(30)
    if key == ord('q') or key == 27:
        exit(-1)
    if(len(thresh)==num_classes):
        break

cv.destroyAllWindows()
# cv.waitKey()
# thresh = [(np.array([1, 219, 130]),np.array([5, 251, 139])),
#           (np.array([45, 70, 107]),np.array([57,112,117])),
#           (np.array([103, 164, 135]),np.array([105, 193, 149])),
#           (np.array([10, 237, 148]),np.array([12, 255, 158]))]
objects = []
color = [(0,0,255),(255,0,0),(0,255,0),(255,255,0)]

while True:
    objects.clear()
    ret, frame = cap.read()
    frame = frame[table_top:table_bottom, table_left:table_right]
    frame_HSV = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    frameHeight = frame.shape[0]
    frameWidth = frame.shape[1]

    for i in range(0,num_classes):
       mask, class_rect = Find_Classes(frame_HSV, thresh[i][0], thresh[i][1])
       Draw_Class(frame, class_rect, color[i%len(color)])
       cv.imshow("Class "+ str(i),mask)
       cv.waitKey(1)
       for rect in class_rect:
            left,top,width,height = rect
            x_new = (frameHeight - (top + height)) / frameHeight
            y_new = left / frameWidth
            width_new = height / frameHeight
            height_new = width / frameWidth
            objects.append(CV_Object(x_new, y_new, width_new, height_new, i))

    objects = remove_collision(objects)
    cv.imshow("Detection",frame)
    key = cv.waitKey(1500)
    Simulator.Organize(objects,table_controller)

    if key == ord('q') or key == 27:
        break


# Destroys all of the HighGUI windows.
cv.destroyAllWindows()

# release the captured frame
cap.release()
